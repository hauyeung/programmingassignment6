using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using XnaCards;

namespace ProgrammingAssignment6
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        const int WINDOW_WIDTH = 800;
        const int WINDOW_HEIGHT = 600;

        // max valid blackjack score for a hand
        const int MAX_HAND_VALUE = 21;

        // deck and hands
        Deck deck;
        List<Card> dealerHand = new List<Card>();
        List<Card> playerHand = new List<Card>();

        // hand placement
        const int TOP_CARD_OFFSET = 100;
        const int HORIZONTAL_CARD_OFFSET = 150;
        const int VERTICAL_CARD_SPACING = 125;

        // messages
        SpriteFont messageFont;
        const string SCORE_MESSAGE_PREFIX = "Score: ";
        Message playerScoreMessage;
        List<Message> messages = new List<Message>();

        // message placement
        const int SCORE_MESSAGE_TOP_OFFSET = 25;
        const int HORIZONTAL_MESSAGE_OFFSET = HORIZONTAL_CARD_OFFSET;
        Vector2 winnerMessageLocation = new Vector2(WINDOW_WIDTH / 2,
            WINDOW_HEIGHT / 2);

        // menu buttons
        Texture2D quitButtonSprite;
        List<MenuButton> menuButtons = new List<MenuButton>();

        // menu button placement
        const int TOP_MENU_BUTTON_OFFSET = TOP_CARD_OFFSET;
        const int QUIT_MENU_BUTTON_OFFSET = WINDOW_HEIGHT - TOP_CARD_OFFSET;
        const int HORIZONTAL_MENU_BUTTON_OFFSET = WINDOW_WIDTH / 2;
        const int VERTICAL_MENU_BUTTON_SPACING = 125;

        // use to detect hand over when player and dealer didn't hit
        bool playerHit = false;
        bool dealerHit = false;

        // game state tracking
        static GameState currentState = GameState.WaitingForPlayer;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // set resolution and show mouse
            graphics.PreferredBackBufferHeight = WINDOW_HEIGHT;
            graphics.PreferredBackBufferWidth = WINDOW_WIDTH;
            this.IsMouseVisible = true;

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // create and shuffle deck
            deck = new Deck(this.Content, 100, 100);
            deck.Shuffle();

            // first player card   
            Card playercard1 = deck.TakeTopCard();
            playercard1.X = 100;
            playercard1.Y = 100;
            playercard1.FlipOver();
            playerHand.Add(playercard1);


            // first dealer card
            Card dealercard1 = deck.TakeTopCard();
            dealercard1.X = WINDOW_WIDTH - 100;
            dealercard1.Y = 100;
            dealerHand.Add(dealercard1);


            // second player card
            Card playercard2 = deck.TakeTopCard();
            playercard2.X = 100;
            playercard2.Y = 200;
            playercard2.FlipOver();
            playerHand.Add(playercard2);


            // second dealer card
            Card dealercard2 = deck.TakeTopCard();
            dealercard2.X = WINDOW_WIDTH - 100;
            dealercard2.Y = 200;
            dealercard2.FlipOver();
            dealerHand.Add(dealercard2);


            // load sprite font, create message for player score and add to list
            messageFont = Content.Load<SpriteFont>("Arial24");
            playerScoreMessage = new Message(SCORE_MESSAGE_PREFIX + GetBlackjackScore(playerHand).ToString(),
                messageFont,
                new Vector2(HORIZONTAL_MESSAGE_OFFSET, SCORE_MESSAGE_TOP_OFFSET));
            messages.Add(playerScoreMessage);

            // load quit button sprite for later use
            quitButtonSprite = Content.Load<Texture2D>("quitbutton");

            // create hit button and add to list
            Vector2 hitbuttonpos = new Vector2(((float)WINDOW_WIDTH / 2), ((float)100));
            Texture2D hitbuttonsprite = Content.Load<Texture2D>("hitbutton");
            MenuButton hitbutton = new MenuButton(hitbuttonsprite, hitbuttonpos, GameState.PlayerHitting);
            menuButtons.Add(hitbutton);

            // create stand button and add to list
            Texture2D standbuttonsprite = Content.Load<Texture2D>("standbutton");
            Vector2 standbuttonpos = new Vector2(((float)WINDOW_WIDTH / 2), ((float)250));
            MenuButton standbutton = new MenuButton(standbuttonsprite, standbuttonpos, GameState.WaitingForDealer);
            menuButtons.Add(standbutton);

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            base.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // update menu buttons as appropriate
            foreach (MenuButton button in menuButtons)
            {
                if (currentState == GameState.WaitingForPlayer || currentState == GameState.DisplayingHandResults)
                {
                    button.Update(Mouse.GetState());
                }
            }


            // game state-specific processing
            messages.Clear();
            playerScoreMessage = new Message(SCORE_MESSAGE_PREFIX + GetBlackjackScore(playerHand).ToString(),
                messageFont,
                new Vector2(HORIZONTAL_MESSAGE_OFFSET, SCORE_MESSAGE_TOP_OFFSET));
            messages.Add(playerScoreMessage);
            switch (currentState)
            {
                case GameState.Exiting:
                    {
                        Exit();
                        break;
                    }
                case GameState.PlayerHitting:
                    {
                        Card card = deck.TakeTopCard();
                        card.X = 100;
                        card.Y = (playerHand.Count + 1) * 100;
                        card.FlipOver();
                        playerHand.Add(card);
                        playerHit = true;
                        currentState = GameState.WaitingForDealer;
                        break;
                    }
                case GameState.DealerHitting:
                    {
                        Card card = deck.TakeTopCard();
                        card.X = WINDOW_WIDTH - 100;
                        card.Y = (dealerHand.Count + 1) * 100;
                        card.FlipOver();
                        dealerHand.Add(card);
                        dealerHit = true;
                        currentState = GameState.CheckingHandOver;
                        break;
                    }
                case GameState.WaitingForDealer:
                    {
                        playerHit = false;
                        if (GetBlackjackScore(dealerHand) <= 16)
                        {
                            currentState = GameState.DealerHitting;
                        }
                        else
                        {
                            dealerHit = false;
                            currentState = GameState.CheckingHandOver;
                        }
                        break;
                    }
                case GameState.CheckingHandOver:
                    {
                        if (GetBlackjackScore(playerHand) > MAX_HAND_VALUE || GetBlackjackScore(dealerHand) > MAX_HAND_VALUE ||
                            (playerHit == false && dealerHit == false))
                        {
                            menuButtons.Clear();
                            

                            currentState = GameState.DisplayingHandResults;
                        }
                        else
                        {
                            currentState = GameState.WaitingForPlayer;
                        }
                        break;
                    }
                case GameState.DisplayingHandResults:
                    {
                        MenuButton quitbutton = new MenuButton(quitButtonSprite, new Vector2(((float)400), ((float)400)), GameState.Exiting);
                        if (!dealerHand.First().FaceUp)
                        {
                            dealerHand.First().FlipOver();
                        }

                        if (GetBlackjackScore(playerHand) > MAX_HAND_VALUE || (GetBlackjackScore(playerHand) > MAX_HAND_VALUE && GetBlackjackScore(dealerHand) > MAX_HAND_VALUE))
                        {
                            Message dealerScoreMessage = new Message(SCORE_MESSAGE_PREFIX + GetBlackjackScore(dealerHand).ToString(),
messageFont,
new Vector2(WINDOW_WIDTH - HORIZONTAL_MESSAGE_OFFSET, SCORE_MESSAGE_TOP_OFFSET));
                            messages.Add(dealerScoreMessage);
                            messages.Add(new Message("Player loses", messageFont, new Vector2(((float)400), ((float)300))));
                            menuButtons.Add(quitbutton);
                        }
                        else if (GetBlackjackScore(dealerHand) > MAX_HAND_VALUE)
                        {
                            Message dealerScoreMessage = new Message(SCORE_MESSAGE_PREFIX + GetBlackjackScore(dealerHand).ToString(),
messageFont,
new Vector2(WINDOW_WIDTH - HORIZONTAL_MESSAGE_OFFSET, SCORE_MESSAGE_TOP_OFFSET));
                            messages.Add(dealerScoreMessage);
                            messages.Add(new Message("Player won!", messageFont, new Vector2(((float)400), ((float)300))));
                            menuButtons.Add(quitbutton);
                        }
                        else
                        {
                            if (GetBlackjackScore(playerHand) == GetBlackjackScore(dealerHand))
                            {
                                Message dealerScoreMessage = new Message(SCORE_MESSAGE_PREFIX + GetBlackjackScore(dealerHand).ToString(),
        messageFont,
        new Vector2(WINDOW_WIDTH - HORIZONTAL_MESSAGE_OFFSET, SCORE_MESSAGE_TOP_OFFSET));
                                messages.Add(dealerScoreMessage);
                                messages.Add(new Message("It's a tie.", messageFont, winnerMessageLocation));
                                menuButtons.Add(quitbutton);
                            }
                            else if (GetBlackjackScore(playerHand) > GetBlackjackScore(dealerHand))
                            {
                                Message dealerScoreMessage = new Message(SCORE_MESSAGE_PREFIX + GetBlackjackScore(dealerHand).ToString(),
        messageFont,
        new Vector2(WINDOW_WIDTH - HORIZONTAL_MESSAGE_OFFSET, SCORE_MESSAGE_TOP_OFFSET));
                                messages.Add(dealerScoreMessage);
                                messages.Add(new Message("Player won!", messageFont, winnerMessageLocation));
                                menuButtons.Add(quitbutton);
                            }
                            else if (GetBlackjackScore(playerHand) < GetBlackjackScore(dealerHand))
                            {
                                Message dealerScoreMessage = new Message(SCORE_MESSAGE_PREFIX + GetBlackjackScore(dealerHand).ToString(),
        messageFont,
        new Vector2(WINDOW_WIDTH - HORIZONTAL_MESSAGE_OFFSET, SCORE_MESSAGE_TOP_OFFSET));
                                messages.Add(dealerScoreMessage);
                                messages.Add(new Message("Player loses", messageFont, winnerMessageLocation));
                                menuButtons.Add(quitbutton);
                            }
                        }
                        break;
                    }


            }



            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Goldenrod);

            spriteBatch.Begin();

            // draw hands
            foreach (Card card in playerHand)
            {
                card.Draw(spriteBatch);
            }

            foreach (Card card in dealerHand)
            {
                card.Draw(spriteBatch);
            }


            // draw messages
            foreach (Message message in messages)
            {
                message.Draw(spriteBatch);
            }


            // draw menu buttons
            foreach (MenuButton button in menuButtons)
            {
                button.Draw(spriteBatch);
            }


            spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Calculates the Blackjack score for the given hand
        /// </summary>
        /// <param name="hand">the hand</param>
        /// <returns>the Blackjack score for the hand</returns>
        private int GetBlackjackScore(List<Card> hand)
        {
            // add up score excluding Aces
            int numAces = 0;
            int score = 0;
            foreach (Card card in hand)
            {
                if (card.Rank != Rank.Ace)
                {
                    score += GetBlackjackCardValue(card);
                }
                else
                {
                    numAces++;
                }
            }

            // if more than one ace, only one should ever be counted as 11
            if (numAces > 1)
            {
                // make all but the first ace count as 1
                score += numAces - 1;
                numAces = 1;
            }

            // if there's an Ace, score it the best way possible
            if (numAces > 0)
            {
                if (score + 11 <= MAX_HAND_VALUE)
                {
                    // counting Ace as 11 doesn't bust
                    score += 11;
                }
                else
                {
                    // count Ace as 1
                    score++;
                }
            }

            return score;
        }

        /// <summary>
        /// Gets the Blackjack value for the given card
        /// </summary>
        /// <param name="card">the card</param>
        /// <returns>the Blackjack value for the card</returns>
        private int GetBlackjackCardValue(Card card)
        {
            switch (card.Rank)
            {
                case Rank.Ace:
                    return 11;
                case Rank.King:
                case Rank.Queen:
                case Rank.Jack:
                case Rank.Ten:
                    return 10;
                case Rank.Nine:
                    return 9;
                case Rank.Eight:
                    return 8;
                case Rank.Seven:
                    return 7;
                case Rank.Six:
                    return 6;
                case Rank.Five:
                    return 5;
                case Rank.Four:
                    return 4;
                case Rank.Three:
                    return 3;
                case Rank.Two:
                    return 2;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Changes the state of the game
        /// </summary>
        /// <param name="newState">the new game state</param>
        public static void ChangeState(GameState newState)
        {
            currentState = newState;
        }
    }
}

